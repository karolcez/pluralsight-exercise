import React, { PropTypes, Component } from 'react';
import { Link, IndexLink } from 'react-router';
import LoadingDots from './LoadingDots';
import { connect } from 'react-redux';

class Header extends Component{
    constructor(props, context) {
        super(props, context);
    }
    render() {
        return (
            <nav>
                <IndexLink to="/" activeClassName="active">Home</IndexLink>
                {" | "}
                <Link to="/courses" activeClassName="active">Courses: {this.props.coursesCount}</Link>
                {" | "}
                <Link to="/about" activeClassName="active">About </Link>
                {this.props.loading && <LoadingDots interval={100} dots={20} />}
            </nav>
        );
    }

};

Header.propTypes = {
    loading: PropTypes.bool.isRequired
};
function mapStateToProps(state, ownProps) {
    return {
        coursesCount: state.courses.length
    };
}
export default connect(mapStateToProps)(Header);