import React from 'react';

class AboutPage extends React.Component {
  render() {
    return (
      <div className="jumbotron">
        <h1>
            About
        </h1>
        <p>
            This application uses REact, redux, react router and a variety of other helpful librares.
        </p>
      </div>
    );
  }
}

export default AboutPage;
