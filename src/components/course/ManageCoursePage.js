import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as courseActions from '../../actions/courseAction';
import CourseForm from './CourseForm';
import toastr from 'toastr';

export class ManageCoursePage extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            course: Object.assign({}, this.props.course),
            errors: {},
            saving: false,
            deleting: false
        };

        this.updateCoursesState = this.updateCoursesState.bind(this);
        this.saveCourse = this.saveCourse.bind(this);
        this.deleteCourse = this.deleteCourse.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        
        
        if (nextProps.course && (this.props.course.id != nextProps.course.id)) {
            this.setState({ course: Object.assign({}, nextProps.course) });
        }
    }

    updateCoursesState(event) {
        const field = event.target.name;
        let course = this.state.course;
        course[field] = event.target.value;
        return this.setState({ course: course });
    }

    courseFormValid(){
        let formIsValid = true;
        let errors = {};

        if(this.state.course.title.length < 5){
            errors.title = 'Title must be at least 5 characters.';
            formIsValid = false;
        }
        
        if(!this.state.course.authorId){
            errors.authorId = 'Select author.';
            formIsValid = false;
        }
        // const reg = new RegExp("^{1,}\:[0-9]{2}$");
        // console.log(reg.test(this.state.course.length));
                
        if(!new RegExp("^[0-9]{1,}\:[0-9]{2}$").test(this.state.course.length)){
            errors.length = 'Correct format should be 00:00.';
            formIsValid = false;
        }        

        this.setState({errors: errors});
        return formIsValid;
    }

    saveCourse(event) {
        event.preventDefault();

        if(!this.courseFormValid())
        {
            return;
        }

        this.setState({saving: true});
        this.props.actions.saveCourse(this.state.course).then(
            () => this.redirect("saving","Course saved")
        ).catch(error => {
            this.setState({saving: false});
            toastr.error(error);
        });
    }
    deleteCourse(event) {
        //event.preventDefault();

        this.setState({deleting: true});
        this.props.actions.deleteCourse(this.state.course).then(
            () => this.redirect("deleting","Course deleted")
        ).catch(error => {
            this.setState({deleting: false});
            toastr.error(error);
        });
    }
    redirect(field, message) {
        const newState = this.state;
        newState[field] = false;
        this.setState(newState);
        toastr.success(message);
        this.context.router.push('/courses');
    }
    render() {
        return (
            <CourseForm
                allAuthors={this.props.authors}
                onChange={this.updateCoursesState}
                onSave={this.saveCourse}
                onDelete={this.deleteCourse}
                course={this.state.course}
                errors={this.state.errors}
                saving={this.state.saving}
                deleting={this.state.deleting}
            />
        );
    }
}
ManageCoursePage.propTypes = {
    course: PropTypes.object.isRequired,
    authors: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

ManageCoursePage.contextTypes = {
    router: PropTypes.object
};

function getCourseById(courses, id) {
    const course = courses.filter(course => course.id == id);
    if (course) return course[0];
    return null;
}
function mapStateToProps(state, ownProps) {
    const courseId = ownProps.params.id;

    let course = { id: '', watchHref: '', title: '', authorId: '', length: '', category: '' };

    if (courseId && state.courses.length > 0) {
        course = getCourseById(state.courses, courseId);
    }

    const authorsFormattedForDropdown = state.authors.map(author => {
        return {
            value: author.id,
            text: author.firstName + ' ' + author.lastName
        };
    });
    return {
        course: course,
        authors: authorsFormattedForDropdown
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(ManageCoursePage);